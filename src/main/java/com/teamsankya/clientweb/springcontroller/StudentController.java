package com.teamsankya.clientweb.springcontroller;

import java.util.Set;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.teamsankya.clientweb.dto.ResponseGenerator;
import com.teamsankya.clientweb.dto.StudentBean;
@Controller
public class StudentController {

	@RequestMapping(path ="/home", method = RequestMethod.GET)
	public String home() {
		return "Home";
	}
	
	
	
	@RequestMapping(value = "/addreq", method = RequestMethod.GET)
	public String addRequest() {
		return "AddStudent";
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String addStudent(StudentBean bean,ModelMap map) {
		Client client = ClientBuilder.newClient();
		WebTarget webTarget = client.target("http://localhost:8081/webservices/api/student/add");
		Response response = webTarget.request().accept(MediaType.APPLICATION_JSON).post(Entity.json(bean));
		ResponseGenerator add=response.readEntity(ResponseGenerator.class);
		System.out.println(add);
		map.addAttribute("add",add);

		return "AddStu";
	}
	
	
	

	@RequestMapping(value = "/delreq", method = RequestMethod.GET)
	public String delelteRequest() {
		return "DeleteStud";
	}

	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public String deleteStudent(ModelMap map, int regno) {
		Client client = ClientBuilder.newClient();
		WebTarget webTarget = client.
				target("http://localhost:8081/webservices/api/student/delete?regno=");
		Response response = webTarget.request().accept(MediaType.APPLICATION_JSON).delete();
		ResponseGenerator del=response.readEntity(ResponseGenerator.class);
		System.out.println(del);
		map.addAttribute("del",del);

return "delete";
		
	}


	@RequestMapping(value = "/searchreq", method = RequestMethod.GET)
	public String searchRequest() {		
	

		return "SearchStudent";
	}

	@RequestMapping(value = "/search", method = RequestMethod.GET)
	public String searchEmployee(ModelMap map, int regno) {
		
		Client client = ClientBuilder.newClient();
		WebTarget webTarget = client.
				target("http://localhost:8081/webservices/api/student/get/102");
		Response response = webTarget.request().accept(MediaType.APPLICATION_JSON).get();
		StudentBean get=response.readEntity(StudentBean.class);
		System.out.println(get);
		map.addAttribute("get", get);

		return "searchStud";
}
	
	
	

	@RequestMapping(value = "/view", method = RequestMethod.GET)
	public String viewAll( ModelMap map) {
		
		
		Client client = ClientBuilder.newClient();
		WebTarget webTarget = client.
				target("http://localhost:8081/webservices/api/student/getAll");
		Response response = webTarget.request().accept(MediaType.APPLICATION_JSON).get();
		Set<StudentBean> view= (Set<StudentBean>) response.readEntity(StudentBean.class);
		//System.out.println(response.readEntity(ResponseGenerator.class));
		
		return "viewAllEmp";
		
		
		
	}
	
	@RequestMapping(value = "/updatereq", method = RequestMethod.GET)
	public String updateRequest() {
		return "Update";
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public String updateStudent(StudentBean bean,ModelMap map) {
		Client client = ClientBuilder.newClient();
		WebTarget webTarget = client.target("http://localhost:8081/webservices/api/student/update");
		Response response = webTarget.request().accept(MediaType.APPLICATION_JSON).put(Entity.json(bean));
		String update=response.readEntity(String.class);
		map.addAttribute("update", update);
	
	return "UpdateSucces";
	}
}